package de.higger.tools.calendar_adapter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CalendarAdapterApplication {

	public static void main(String[] args) {
		SpringApplication.run(CalendarAdapterApplication.class, args);
	}

}
