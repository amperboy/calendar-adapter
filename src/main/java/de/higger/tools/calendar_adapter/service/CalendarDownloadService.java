package de.higger.tools.calendar_adapter.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URL;

@Service
public class CalendarDownloadService {

    private static final Logger LOGGER = LoggerFactory.getLogger(CalendarDownloadService.class);

    @Value("${application.calendar.sourceEndpoint}")
    private String calendarEndpoint;

    public void downloadService(OutputStream outputStream) throws IOException {

        LOGGER.info("Fetch calendar from {} ", calendarEndpoint);

        try (BufferedInputStream in = new BufferedInputStream(new URL(calendarEndpoint).openStream())) {

            String icsData = new String(in.readAllBytes());
            icsData = icsData.replaceAll("\r", "");
            icsData = icsData.replaceAll("\n", "\r\n");
            icsData = icsData.replaceAll("\r\nDTSTART", "\r\nCLASS:PUBLIC\r\nDTSTART");
            icsData = icsData.replaceAll("X-WR-.+\r\n .*\r\n","");
            icsData = icsData.replaceAll("X-WR-.+\r\n","");

            outputStream.write(icsData.getBytes());
        }

        LOGGER.info("Calendar streamed to outputStream");
    }
}
