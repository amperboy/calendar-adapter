package de.higger.tools.calendar_adapter.api;

import de.higger.tools.calendar_adapter.service.CalendarDownloadService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.mvc.method.annotation.StreamingResponseBody;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;

@RestController
@RequestMapping("/api")
public class CalendarResource {

    @Autowired
    private CalendarDownloadService calendarDownloadService;

    @ModelAttribute
    public void setResponseHeader(HttpServletResponse response) {
		response.setHeader("Cache-Control", "no-cache");
		response.setHeader("Content-Type", "text/calendar");
    }


    @GetMapping(value = "/calendar.ics", produces = "text/calendar")
    public StreamingResponseBody downloadService() throws IOException {

        return new StreamingResponseBody() {
            @Override
            public void writeTo(OutputStream outputStream) throws IOException {
                calendarDownloadService.downloadService(outputStream);
            }
        };
    }

}
