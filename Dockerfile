FROM openjdk:14-alpine
ENV application.calendar.sourceEndpoint=http://foo.de/source/ical

EXPOSE 8080

WORKDIR /application
COPY target/unzip/BOOT-INF/lib /application/lib
COPY target/unzip/META-INF /application/META-INF
COPY target/unzip/BOOT-INF/classes /application

CMD ["sh","-c","java -cp .:lib/* de.higger.tools.calendar_adapter.CalendarAdapterApplication"]
